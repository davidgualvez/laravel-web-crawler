<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Goutte\Client;
use App\Services\CrawlMyBenta;
use Image;
use Storage;
use App\Models\MyBenta;
use App\Models\MyBentaImages;

class ScrapeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:mybenta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'MyBenta Scrapping';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        $client = new Client();
        $counter = 1; 

        $crawler = $client->request('GET', config('webscrapper.mybenta'));        // MYBENTA

        
        // Storage::delete('/public/mybenta'); 
        $crawler->filter('.panel-white')->each(function ($node) { 
            $base = "https://www.mybenta.com";

            $image  = $base.$node->filter('div.uk-grid.uk-grid-small > div:nth-child(1) > a > img')->attr('data-src');
            $title  = $node->filter('div.uk-grid.uk-grid-small > div:nth-child(2) > a > h1 > strong')->text();
            $desc   = $node->filter('div.uk-grid.uk-grid-small > div:nth-child(2) > a > p:nth-child(2)')->text();
            $price  = $node->filter('div.uk-grid.uk-grid-small > div:nth-child(3) > center > span')->text();
            $seller = $node->filter('div.uk-grid.uk-grid-small > div:nth-child(3) > center > a > .adSeller > b')->text();

            $internal_link = $node->filter('div.uk-grid.uk-grid-small > div:nth-child(2) > a')->attr('href');
 
            print "\n================================================\n";
            print "\n[IMAGE]        \n{$image} \n";
            print "\n[TITLE]        \n{$title} \n";
            print "\n[DESCRIPTION]  \n{$desc} \n";
            print "\n[PRICE]        \n{$price} \n";
            print "\n[SELLER]       \n{$seller} \n";
            print "\n[INT_LINK]       \n{$internal_link} \n";
 
            $filename = now()->timestamp.'.png';
            $mybenta = MyBenta::create([
                'system_image_path' => $this->storeImage( $image, $filename ),
                'orig_image_path'   => $image,
                'title'             => $title,
                'desc'              => $desc,
                'price'             => str_replace( ',', '', $price ),
                'seller'            => $seller,
                'images'            => $filename
            ]);


            // fetching other images
            $client = new Client();
            $crawler2 = $client->request('GET', $base.$internal_link);
            print "\n";
            print "[ INNER IMAGE COUNT ".$crawler2->filter('.uk-flex.uk-flex-center > div > a')->count()." ]\n";
            if( $crawler2->filter('.uk-flex.uk-flex-center > div > a')->count() > 0 ){


                $contact_number = $crawler2->filter('span.uk-text-bold.uk-text-large.uk-text-primary')->text();
                $mybenta->update(['contact_number' => $contact_number]);
                
                $crawler2->filter('.uk-flex.uk-flex-center > div > a')->each(function ($node2) use ($mybenta, $base) {

                    $orig = $base.$node2->attr('href'); 

                    $filename = now()->timestamp.'.png';
                    $mybenta_images = MyBentaImages::create([
                        'my_bentas_id'          => $mybenta->id,
                        'system_image_path'     => $this->storeImage( $orig, $filename ),
                        'orig_image_path'       => $orig, 
                    ]); 

                    $mybenta->images = $mybenta->images.', '.$filename;
                    $mybenta->save();

                    print "{$orig}\n";
                });

            }
            
        });

        print "\n"; 
    }

    public function storeImage($image_url, $filename = 'test.png'){
        
        $img = Image::make($image_url); 
        $canvas = Image::canvas(760, 410, '#eeeeee');
        $img = $canvas->insert($img, 'center');
        $img->stream(); 

        $img_path = '/public/mybenta/'.$filename;  
        Storage::put( $img_path, $img->getEncoded() ); 

        return $img_path;
    }
}
