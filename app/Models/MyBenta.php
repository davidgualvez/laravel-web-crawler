<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MyBenta extends Model
{
    //
    protected $fillable = [
        'system_image_path',
        'orig_image_path',
        'title',
        'desc',
        'price',
        'seller',
        'contact_number',
        'images'
    ];
}
