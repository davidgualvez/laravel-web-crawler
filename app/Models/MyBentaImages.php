<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MyBentaImages extends Model
{
    //
    protected $fillable = [
        'my_bentas_id',
        'system_image_path',
        'orig_image_path', 
    ];
}
