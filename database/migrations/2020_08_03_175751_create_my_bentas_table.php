<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMyBentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_bentas', function (Blueprint $table) {
            $table->id();
            $table->text('system_image_path')->nullable();
            $table->text('orig_image_path')->nullable(); 
            $table->string('title')->nullable();
            $table->text('desc')->nullable();
            $table->double('price')->nullable();
            $table->string('seller')->nullable();
            $table->string('contact_number')->nullable();
            $table->text('images');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_bentas');
    }
}
