<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


use Goutte\Client;
Route::get('/start-crawl', function(){


    $client = new Client();
 
    // $crawler = $client->request('GET', 'https://www.bbc.com/news/uk-46935721');      // BBC
    // $crawler = $client->request('GET', 'https://www.carousell.ph/');                 // CAROUSELL
    // $crawler = $client->request('GET', 'https://cnnphilippines.com/');               // CNN PH
    $crawler = $client->request('GET', 'https://www.mybenta.com/search?cat=16');        // MYBENTA
    // dd($crawler);
    
    /**
     * BBC
     */
    // $crawler->filter('.dkyUCQEdrH  ._2I6wMfj_I-')->each(function ($node) {
    //     var_dump($node->text()."\n"); 
    //     // dd( $node->text() );
    // });
 
    /**
     * Carousell
     */
    // $crawler->filter('.story-body__inner')->each(function ($node) {
    //     var_dump($node->text()."\n");
    //     // dd($node);
    // });

    // $crawler->filter('body > div > div > div > div:nth-child(4) > div:nth-child(1) > section > div:nth-child(2) > div > div')->each(function ($node) {
    //     var_dump($node->text()."\n"); 
    //     // dd( $node->text() );
    // });

    /**
     * 
     * CNN
     * 
     */
    // $crawler->filter('.cbwidget-list > li > a')->each(function ($node) {
    //     dd($node->text()."\n"); 
    // });

    /**
     * MYBENTA
     */
    $crawler->filter('.panel-white')->each(function ($node) {
        $title = $node->filter('div.uk-grid.uk-grid-small > div:nth-child(2) > a > h1 > strong')->text();
        $desc = $node->filter('div.uk-grid.uk-grid-small > div:nth-child(2) > a > p:nth-child(2)')->text();

        var_dump( 
            $title,
            $desc
        );
        
    });



})->name('crawl');
