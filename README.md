## Setup

> git clone this repository

Run Composer command
> composer install

Create ENV File from Example and update DB Credential
> cp .env.example .env

Generate Key to run the app without an error
> php artisan key:generate

Cache the env into config
> php artisan config:cache

Create Storage Link
> php artisan storage:link

Run DB Migration
> php artisan migrate


## Running Scrapper for supported sites

> php artisan scrape:mybenta
